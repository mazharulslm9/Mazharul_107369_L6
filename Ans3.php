<?php

$arr = array('1', '2', '3', '4', '5');
echo 'Original array : <br>';
foreach ($arr as $x) {
    echo "$x ";
}
$put = '$';
array_splice($arr, 3, 0, $put);
echo " <br> After inserting '$' the array is :<br>";
foreach ($arr as $newArray) {
    echo "$newArray ";
}